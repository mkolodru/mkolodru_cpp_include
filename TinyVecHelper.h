#ifndef TINY_VECTOR_HELPER_H
#define TINY_VECTOR_HELPER_H

#include <iostream>
#include <blitz/array.h>
#include <blitz/tinyvec-et.h>
#include "RandomClass.h"
#include <cmath>

#define NDIM 3

using namespace blitz;

typedef TinyVector<double,NDIM> dVec;
typedef TinyVector<int,NDIM> iVec;

double mag_sq(const dVec & dv) {
  return dot(dv,dv);
} 

int mag_sq(const iVec & iv) {
  return dot(iv,iv);
} 

double mag(const dVec & dv) {
  return sqrt(mag_sq(dv));
}

double mag(const iVec & iv) {
  return sqrt(double(mag_sq(iv)));
}

bool operator == (const iVec & a, const iVec & b) {
  for(int dim=0;dim<NDIM;dim++)
    if(a[dim]!=b[dim])
      return false;
  return true;
}

bool operator == (const dVec & a, const dVec & b) {
  for(int dim=0;dim<NDIM;dim++)
    if(a[dim]!=b[dim])
      return false;
  return true;
}

bool operator != (const dVec & a, const dVec & b) {
  return !(a==b);
}

bool operator != (const iVec & a, const iVec & b) {
  return !(a==b);
}

ostream & operator << (ostream & out, const dVec & dv) {
  for(int dim=0;dim<NDIM;dim++)
    out << dv[dim] << ' ';
  return out;
}

ostream & operator << (ostream & out, const iVec & iv) {
  for(int dim=0;dim<NDIM;dim++)
    out << iv[dim] << ' ';
  return out;
}

istream & operator >> (istream & in, dVec & dv) {
  for(int dim=0;dim<NDIM;dim++)
	in >> dv[dim];
  return in;
}

istream & operator >> (istream & in, iVec & iv) {
  for(int dim=0;dim<NDIM;dim++)
	in >> iv[dim];
  return in;
}

bool operator < (const iVec & lhs, const iVec & rhs) {
  int lSz=mag_sq(lhs), rSz=mag_sq(rhs), dim;
  if(lSz<rSz)
    return true;
  if(lSz>rSz)
    return false;
  for(dim=NDIM-1;dim>=0;dim--) {
    if(lhs[dim]<rhs[dim])
      return true;
    if(rhs[dim]<lhs[dim])
      return false;
  }
  return false;
}

bool operator < (const dVec & lhs, const dVec & rhs) {
  return mag_sq(lhs)< mag_sq(rhs);
}

bool dVecLessThan(const dVec & a, const dVec & b) {
  return a < b;
}

struct dVecCompare {
  bool operator () (const dVec & lhs, const dVec & rhs){
    return lhs < rhs;
  }
};

bool iVecLessThan(const iVec & a, const iVec & b) {
  return a < b;
}

struct iVecCompare {
  bool operator () (const iVec & lhs, const iVec & rhs){
    return lhs < rhs;
  }
};

//Pick dVec at radius r in a spherically symmetric way
void pickDVecSphSymm(double r, RandomClass * randomGen, dVec & dv) {
  double theta,phi;
  theta=acos(2*randomGen->ranf()-1);
  phi=2*M_PI*randomGen->ranf();
  dv[0]=r*sin(theta)*cos(phi);
  dv[1]=r*sin(theta)*sin(phi);
  dv[2]=r*cos(theta);
}

//Pick dv from a spherically symmetric distribution where amplitude A(r)
//is given by A(r)~f(r)/r^2. FInv will be the inverse of the cumulative 
//distribution function F(r)=integral(f(r) dr).  The bounds r1 and r2 on
//r are set by passing F1=F(r1) and F2=F(r2)
void pickDVec(double F1, double F2, double (*FInv)(double), RandomClass * randomGen, dVec & dv) {
  assert(NDIM==3);
  double r=FInv(randomGen->ranf()*(F2-F1)+F1);
  pickDVecSphSymm(r,randomGen,dv);
}

//For picking holes, A=const, so f=x^2, F=x^3, Finv=x^(1/3)
double flatFInv(double x) {
  return pow(x,1.0/3.0);
}

//For picking particles, A=1/r^2, so f=1, F=x, Finv=x
double invSqFInv(double x) {
  return x;
}

//For picking particles, A=1/r^4, so f=1/r^2, F=-1/x, Finv=-1/x
double invQuartFInv(double x) {
  return -1.0/x;
}



#endif
