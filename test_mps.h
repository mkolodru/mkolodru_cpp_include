#ifndef TEST_MPS_H
#define TEST_MPS_H

#include <iostream>
#include <fstream>
#include <Eigen/Eigen>
#include <Eigen/Dense>
#include "MPSobcClass.h"
#include "mkbasic.h"
#include "mkmath.h"
#include "mklinalg.h"
#include <complex>

using namespace std;
using namespace Eigen;

ostream & operator << (ostream & out, const MatrixXcd & m) {
  //  out << m.rows()<<'x'<<m.cols()<<endl;
  for(int j=0;j<m.rows();j++) {
	for(int k=0;k<m.cols();k++) {
	  out << m(j,k) << ' ';
	}
	out << endl;
  }
  return out;
}

void test_matrix_equiv(const MatrixXcd & m1, const MatrixXcd & m2,double tol=1e-6) {
  assert(m1.rows()==m2.rows()&&m1.cols()==m2.cols());
  MatrixXcd diff=m1-m2;
  double max_val=diff.lpNorm<Infinity>();
  if(max_val>tol) {
	cerr << "Matrices differ by more than they should" << endl;
	cerr << "m1=" << endl;
	cerr << m1 << endl;
	cerr << "m2=" << endl;
	cerr << m2 << endl;
	cerr << "Maximum difference=" << max_val << endl;
	exit(1);
  }
}

//Check that eH is the same as exp(mult*H), where H is from Hfile
void test_exp_H(MPSobcClass<complex<double> > & eH, const vector<vector<int> > & basis, string Hfile, complex<double> mult) {
  cerr << "In test_exp_H with Hfile=" << Hfile << endl;

  MatrixXcd M1,M2;
  MatrixXd H;
  int j,k;

  //Read H from file and exponentiate it
  int NCONFIG;
  ifstream Hin(Hfile.c_str());
  Hin >> NCONFIG;
  assert(NCONFIG==basis.size());
  H=MatrixXd::Zero(NCONFIG,NCONFIG);
  for(j=0;j<NCONFIG;j++)
	for(k=0;k<NCONFIG;k++)
	  Hin >> H(j,k);
  M1=H.cast<complex<double> >();
  M1=exp<complex<double> >(M1,mult);

  //Read off values of eH
  eH.allVals(basis,M2);

  //Check that they match
  test_matrix_equiv(M1,M2);
}

void test_obs(MPSobcClass<complex<double> > & obs, const vector<vector<int> > & basis, string obsfile) {
  cerr << "In test_obs obsfile=" << obsfile << endl;

  MatrixXcd M1,M2;
  MatrixXd obsM;
  int j,k;

  //Read obs from file
  int NCONFIG;
  ifstream obsin(obsfile.c_str());
  obsin >> NCONFIG;
  assert(NCONFIG==basis.size());
  obsM=MatrixXd::Zero(NCONFIG,NCONFIG);
  for(j=0;j<NCONFIG;j++)
	for(k=0;k<NCONFIG;k++)
	  obsin >> obsM(j,k);
  M1=obsM.cast<complex<double> >();

  //Read off values of MPS obs
  obs.allVals(basis,M2);

  //Check that they match
  test_matrix_equiv(M1,M2);
}

void test_apply_mpo_to_mps(const vector<vector<int> > & basis, int numSites) {
  MPSobcClass<complex<double> > mpo, mps;
  mpo.InitMPO(numSites,4,2);
  mps.InitMPS(numSites,4,2);
  mpo.pickRandomMPS();
  mps.pickRandomMPS();

  MatrixXcd M1;
  VectorXcd psi1,psi2;
  mpo.allVals(basis,M1);
  mps.allVals(basis,psi1);

  mps.applyMPO(mpo);
  mps.allVals(basis,psi2);

  eout(M1);
  eout(psi1);
  eout(psi2);

  test_matrix_equiv(M1*psi1,psi2);
}

void test_apply_mpo_to_mpo(const vector<vector<int> > & basis, int numSites) {
  MPSobcClass<complex<double> > mpo1, mpo2;
  mpo1.InitMPO(numSites,4,2);
  mpo2.InitMPO(numSites,4,2);
  mpo1.pickRandomMPS();
  mpo2.pickRandomMPS();

  MatrixXcd M1,M2,M3;
  mpo1.allVals(basis,M1);
  mpo2.allVals(basis,M2);

  mpo2.applyMPO(mpo1);
  mpo2.allVals(basis,M3);

  test_matrix_equiv(M1*M2,M3);
}


#endif
