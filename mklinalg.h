#ifndef MK_LINALG_H
#define MK_LINALG_H

#include <Eigen/Eigen>
using namespace Eigen;

template<class MatrixType> 
MatrixType outer(const MatrixType & A, const MatrixType & B) {
  MatrixType mtemp(A.rows()*B.rows(),A.cols()*B.cols());
  for(int j=0;j<A.rows();j++)
	for(int k=0;k<A.cols();k++)
	  mtemp.block(j*B.rows(),k*B.cols(),B.rows(),B.cols())=A(j,k)*B;
  return mtemp;
}

//Return exp(c*H), where H must be self-adjoint
template<class scalar_type> 
Matrix<scalar_type,Dynamic,Dynamic> exp(const Matrix<scalar_type,Dynamic,Dynamic> & H, scalar_type c) {
  assert(H.rows()==H.cols());

  typedef Matrix<scalar_type,Dynamic,Dynamic> MatrixType;
  typedef Matrix<scalar_type,Dynamic,1> VectorType;

  //Do it the diagonalization way
  SelfAdjointEigenSolver<MatrixType> eigsolve(H);
  MatrixType U=eigsolve.eigenvectors();
  //  MatrixXd D=eigsolve.eigenvalues().asDiagonal();
  VectorXd Ed=eigsolve.eigenvalues();
  VectorType E=(Ed.cast<scalar_type>()*c).cwise().exp();
  //  MatrixType diff2=H-U*D*U.adjoint(); == 0 by testing
  //  If H=U D Udag, then exp(c H) = U exp(c D) Udag
  MatrixType result=U*E.asDiagonal()*U.adjoint();

  /*
  MatrixType diff=result-result_old;
  cout << "m=" << endl;
  cout << m << endl;
  cout << "diff=" << endl;
  cout << diff << endl;
  cout << "result=" << endl;
  cout << result << endl;
  cout << "result_old=" << endl;
  cout << result_old << endl;

  exit(1);
  */

  return result;
}

#endif
