#ifndef MPS_OBC_CLASS_H
#define MPS_OBC_CLASS_H

#include <fstream>
#include <Eigen/Dense>
#include "mkmath.h"
#include "mklinalg_eigen3.h"
#include "MKSVD_eigen3.h"

using namespace std;
using namespace Eigen;

template <class scalar_type>
class MPSobcClass
{

public:
  int numSites;
  int bondDimension;
  int spinSize, mposs;
  bool leftToRight, isCanonical, isNormalized, isMPO;
  //If representing MPO as MPS, MM[s1][s2]->M[s1+mposs*s2]
  typedef Matrix<scalar_type,Eigen::Dynamic,Eigen::Dynamic> MatrixType;
  typedef Matrix<scalar_type,Eigen::Dynamic,1> VectorType;
  vector< vector< MatrixType > > M;
  //  JacobiSVD<MatrixType> svd;
  MKSVD<scalar_type> svd;

  void computeSVD(const MatrixType & Mtemp) {
	svd.compute(Mtemp);//, ComputeThinU | ComputeThinV);
  }

  void InitMPS(int t_numSites,int t_bondDimension, int t_spinSize)
  {
    numSites=t_numSites;
    bondDimension=t_bondDimension;
	spinSize=t_spinSize;
	mposs=0;
	isMPO=false;
	InitM();
  }

  void InitMPO(int t_numSites,int t_bondDimension, int t_spinSize)
  {
    numSites=t_numSites;
    bondDimension=t_bondDimension;
	mposs=t_spinSize;
	spinSize=mposs*mposs;
	isMPO=true;
	InitM();
  }

  void InitM() {
	leftToRight=true;
	isCanonical=false;
	isNormalized=false;
    M.resize(spinSize);
    for (int s=0;s<spinSize;s++){
      M[s].resize(numSites);
      for (int site=1;site<numSites-1;site++){
		M[s][site]=MatrixType::Zero(bondDimension,bondDimension);
      }
	  M[s][0]=MatrixType::Zero(1,bondDimension);//Site 0 is row vector
	  M[s][numSites-1]=MatrixType::Zero(bondDimension,1);//Site L-1 is col vector
    }
  }

  void WriteMPS(ostream & outfile)
  {
    outfile<<spinSize<<" "<<numSites<<" "<<bondDimension<<" "<<mposs<<" "<<leftToRight<<" "<<isCanonical<<" "<<isNormalized<<" "<<isMPO<<endl;

    for (int s=0;s<spinSize;s++){
      for (int site=0;site<numSites;site++){
		for (int i=0;i<M[s][site].rows();i++){
		  for (int j=0;j<M[s][site].cols();j++){
			outfile<<M[s][site](i,j)<<" ";
		  }
		  outfile<<endl;
		}
      }
    }
  }

  void ReadMPS(string fileName)
  {
    ifstream infile;
    infile.open(fileName.c_str());
    assert(infile);
	assert(!infile.eof());
	int ss,ns,bd,mss;
	bool lr,ic,in,im;
    infile>>ss>>ns>>bd>>mss>>lr>>ic>>in>>im;
	if(im)
	  InitMPO(ns,bd,mss);
	else
	  InitMPS(ns,bd,ss);
	leftToRight=lr;
	isCanonical=ic;
	isNormalized=in;
	isMPO=im;
    for (int s=0;s<spinSize;s++){
      for (int site=0;site<numSites;site++){
		for (int i=0;i<M[s][site].rows();i++){
		  for (int j=0;j<M[s][site].cols();j++){
			infile>>M[s][site](i,j);
		  }
		}
      }
    }
	infile.close();
  }

  //|this> = |this> + |b>
  void add(const MPSobcClass & b) {
	assert(spinSize==b.spinSize);
	assert(numSites==b.numSites);
	int newDim=b.bondDimension+bondDimension;
	int j,k;
	MatrixType tempM;
	for(int site=0;site<numSites;site++) {
	  for(int spin=0;spin<spinSize;spin++) {
		tempM=MatrixType::Zero(newDim,newDim);
		if(site==0) {
		  assert(M[spin][site].rows()==1&&M[spin][site].cols()==bondDimension);
		  assert(b.M[spin][site].rows()==1&&b.M[spin][site].cols()==b.bondDimension);
		  tempM.block(0,0,1,bondDimension)=M[spin][site];
		  tempM.block(0,bondDimension,1,b.bondDimension)=b.M[spin][site];
		  M[spin][site]=tempM.block(0,0,1,newDim);
		} else if (site==numSites-1) {
		  assert(M[spin][site].cols()==1&&M[spin][site].rows()==bondDimension);
		  assert(b.M[spin][site].cols()==1&&b.M[spin][site].rows()==b.bondDimension);
		  tempM.block(0,0,bondDimension,1)=M[spin][site];
		  tempM.block(bondDimension,0,b.bondDimension,1)=b.M[spin][site];
		  M[spin][site]=tempM.block(0,0,newDim,1);
		} else {
		  assert(M[spin][site].cols()==bondDimension&&M[spin][site].rows()==bondDimension);
		  assert(b.M[spin][site].cols()==b.bondDimension&&b.M[spin][site].rows()==b.bondDimension);
		  tempM.block(0,0,bondDimension,bondDimension)=M[spin][site];
		  tempM.block(bondDimension,bondDimension,b.bondDimension,b.bondDimension)=b.M[spin][site];
		  M[spin][site]=tempM.block(0,0,newDim,newDim);
		}
	  }
	}
	bondDimension=newDim;
	isCanonical=false;
  }

  //Multiply state by a constant val, for getting -tau*H|psi>
  void multiplyBy(scalar_type val) {
	int site=(leftToRight)?0:(numSites-1);
	for(int spin=0;spin<spinSize;spin++)
	  M[spin][site]*=val;
	isCanonical=false;
	isNormalized=false;
  }

  void print_dims(ostream & out) {
	for(int site=0;site<numSites;site++) {
	  for(int spin=1;spin<spinSize;spin++) 
		assert(M[spin][site].rows()==M[0][site].rows()&&M[spin][site].cols()==M[0][site].cols());
	  out << '(' << M[0][site].rows() << 'x' << M[0][site].cols() << ')';
	  out.flush();
	}
	out << endl;
  }

  double zipIt(double error_thresh) {
	MatrixType Mtemp, Mtemp2;
	VectorType tempvec;
	VectorXd singularVec;
	int singularSz;
	int j, k, site, s1, s2;
	int newDim=1;
	double error=0;

	int startsite=0;
	int endsite=numSites-1;
	int delta=1;
	if(!leftToRight) {
	  startsite=numSites-1;
	  endsite=0;
	  delta=-1;
	}
	
	for(site=startsite;site+delta<numSites&&site+delta>=0;site+=delta) {
	  //Interleave the spins so that zeros will be at the bottom and left edge of matrices
	  if(leftToRight) {
		Mtemp.resize(M[0][site].rows()*spinSize,M[0][site].cols());
		for(j=0;j<M[0][site].rows();j++)
		  for(s1=0;s1<spinSize;s1++)
			Mtemp.row(j*spinSize+s1)=M[s1][site].row(j);
	  }
	  else {
		Mtemp.resize(M[0][site].rows(),M[0][site].cols()*spinSize);
		for(j=0;j<M[0][site].cols();j++)
		  for(s1=0;s1<spinSize;s1++)
			Mtemp.col(j*spinSize+s1)=M[s1][site].col(j);
	  }

	  computeSVD(Mtemp);
	  
	  //Adjust next site
	  singularVec=svd.singularValues();
	  singularSz=singularVec.rows();//svd.nonzeroSingularValues();
	  for(j=0;j+1<singularSz;j++) {
		if(!(singularVec(j)>=singularVec(j+1))) {
		  eout(singularVec(j));
		  eout(singularVec(j+1));
		  eout(singularSz);
		}
		  
		assert(singularVec(j)>=singularVec(j+1));
	  }
	  tempvec=singularVec.cast<scalar_type>();
	  double renorm=0, error_curr;
	  for(j=0;j<singularSz;j++) {
		error_curr=ipow(singularVec[j],2);
		if(error_curr<error_thresh)
		  break;
		renorm+=error_curr;
	  }
	  int redDim=j;
	  error_curr=0;
	  for(j=redDim;j<singularSz;j++) {
		error_curr+=ipow(singularVec(j),2);
	  }
	  error+=error_curr;
	  renorm=sqrt(renorm);
	  double nsum=0;
	  for(j=0;j<redDim;j++) {
		singularVec[j]/=renorm;
		nsum+=ipow(singularVec[j],2);
	  }
	  assert(abs(nsum-1) < 1e-9);

	  if(redDim > newDim)
		newDim=redDim;

	  if(leftToRight) { 
		Mtemp=(tempvec.asDiagonal()*svd.matrixV().adjoint()).block(0,0,redDim,M[0][site].cols());
		for(s1=0;s1<spinSize;s1++)
		  M[s1][site+delta]=Mtemp*M[s1][site+delta];
	  } else {
		Mtemp=(svd.matrixU()*tempvec.asDiagonal()).block(0,0,M[0][site].rows(),redDim);
		for(s1=0;s1<spinSize;s1++)
		  M[s1][site+delta]=M[s1][site+delta]*Mtemp;
	  }
	  
	  if(leftToRight) {
		Mtemp=svd.matrixU().block(0,0,M[0][site].rows()*spinSize,redDim);
		for(s1=0;s1<spinSize;s1++) {
		  M[s1][site].resize(M[0][site].rows(),redDim);
		  for(j=0;j<M[0][site].rows();j++)
			M[s1][site].row(j)=Mtemp.row(j*spinSize+s1);
		} 
	  } else {
		Mtemp=svd.matrixV().adjoint().block(0,0,redDim,M[0][site].cols()*spinSize);
		for(s1=0;s1<spinSize;s1++) {
		  M[s1][site].resize(redDim,M[0][site].cols());
		  for(j=0;j<M[0][site].cols();j++)
			M[s1][site].col(j)=Mtemp.col(j*spinSize+s1);
		}
	  }
	}

	if(site!=endsite) {
	  cerr << "After loop, site=" << site << ", endsite=" << endsite << endl;
	  assert(site==endsite);
	}

	bondDimension=newDim;

	site=0;
	for(s1=0;s1<spinSize;s1++) {
	  Mtemp=MatrixType::Zero(1,bondDimension);
	  Mtemp.block(0,0,1,M[s1][site].cols())=M[s1][site];
	  M[s1][site]=Mtemp;
	}
	site=numSites-1;
	for(s1=0;s1<spinSize;s1++) {
	  Mtemp=MatrixType::Zero(bondDimension,1);
	  Mtemp.block(0,0,M[s1][site].rows(),1)=M[s1][site];
	  M[s1][site]=Mtemp;
	}
	for(site=1;site<numSites-1;site++) {
	  for(s1=0;s1<spinSize;s1++) {
		Mtemp=MatrixType::Zero(bondDimension,bondDimension);
		Mtemp.block(0,0,M[s1][site].rows(),M[s1][site].cols())=M[s1][site];
		M[s1][site]=Mtemp;
	  }
	}

	isCanonical=true;

	//Canonize the state
	double norm=0;
	for(s1=0;s1<spinSize;s1++) {
	  if(!leftToRight)
		Mtemp=M[s1][endsite]*M[s1][endsite].adjoint();
	  else
		Mtemp=M[s1][endsite].adjoint()*M[s1][endsite];
	  assert(Mtemp.rows()==1&&Mtemp.cols()==1);
	  norm+=abs(Mtemp(0,0));
	}
	norm=sqrt(norm);
	for(s1=0;s1<spinSize;s1++)
	  M[s1][endsite]=M[s1][endsite]/norm;

	isCanonical=true;
	isNormalized=true;

	leftToRight=!leftToRight;
	return 2*(1-sqrt(1-error));
  }

  double zipIt(int newDim, vector<vector<double> > & rhoReduced) {
	MatrixType Mtemp, Mtemp2;
	VectorType tempvec;
	VectorXd singularVec;
	int singularSz;
	int j, k, site, s1, s2;
	double error=0;

	assert(rhoReduced.size() >= numSites-1);
	assert(newDim>=spinSize);

	int startsite=0;
	int endsite=numSites-1;
	int delta=1;
	if(!leftToRight) {
	  startsite=numSites-1;
	  endsite=0;
	  delta=-1;
	}
	
	for(site=startsite;site+delta<numSites&&site+delta>=0;site+=delta) {
	  //	  cerr << "At site " << site << endl;
	  int rrsite=site;
	  if(!leftToRight)
		rrsite--;

	  if(site==startsite) {
		if(leftToRight)
		  assert(M[0][site].rows()==1&&M[0][site].cols()==bondDimension);
		else
		  assert(M[0][site].rows()==bondDimension&&M[0][site].cols()==1);
	  }
	  else if(leftToRight)
		assert(M[0][site].rows()==newDim&&M[0][site].cols()==bondDimension);
	  else
		assert(M[0][site].cols()==newDim&&M[0][site].rows()==bondDimension);

	  //	  cerr << "Here a" << endl;

	  //Interleave the spins so that zeros will be at the bottom and left edge of matrices
	  if(leftToRight) {
		Mtemp.resize(M[0][site].rows()*spinSize,M[0][site].cols());
		for(j=0;j<M[0][site].rows();j++)
		  for(s1=0;s1<spinSize;s1++)
			Mtemp.row(j*spinSize+s1)=M[s1][site].row(j);
	  }
	  else {
		Mtemp.resize(M[0][site].rows(),M[0][site].cols()*spinSize);
		for(j=0;j<M[0][site].cols();j++)
		  for(s1=0;s1<spinSize;s1++)
			Mtemp.col(j*spinSize+s1)=M[s1][site].col(j);
	  }

	  //	  cerr << "Here b" << endl;

	  computeSVD(Mtemp);
	  
	  //	  cerr << "Here c" << endl;

	  //Adjust next site
	  singularVec=svd.singularValues();
	  singularSz=singularVec.rows();//svd.nonzeroSingularValues();
	  //	  tempvec=VectorType::Zero(singularVec.rows());
	  for(j=0;j+1<singularSz;j++)
		assert(singularVec(j)>=singularVec(j+1));
	  /*	  for(j=0;j<singularVec.rows();j++)
		tempvec(j,0)=(scalar_type)singularVec(j,0); */
	  tempvec=singularVec.cast<scalar_type>();
	  if(rhoReduced[rrsite].size()<newDim)
		rhoReduced[rrsite].resize(newDim);
	  for(j=0;j<newDim&&j<singularSz;j++)
		rhoReduced[rrsite][j]=ipow(singularVec(j),2);
	  for(j=singularSz;j<newDim;j++)
		rhoReduced[rrsite][j]=0;
	  for(j=newDim;j<singularSz;j++)
		error+=ipow(singularVec(j),2);

	  //	  cerr << "Here d" << endl;

	  //Multiply the next site
	  if(site==startsite) {
		if(leftToRight) {
		  Mtemp=MatrixType::Zero(newDim,bondDimension);
		  Mtemp.block(0,0,spinSize,bondDimension)=(tempvec.asDiagonal()*svd.matrixV().adjoint());
		  for(s1=0;s1<spinSize;s1++)
			M[s1][site+delta]=Mtemp*M[s1][site+delta];
		} else {
		  Mtemp=MatrixType::Zero(bondDimension,newDim);
		  Mtemp.block(0,0,bondDimension,spinSize)=(svd.matrixU()*tempvec.asDiagonal());
		  for(s1=0;s1<spinSize;s1++)
			M[s1][site+delta]=M[s1][site+delta]*Mtemp;
		}
	  } else if(leftToRight) { 
		Mtemp=(tempvec.asDiagonal()*svd.matrixV().adjoint()).block(0,0,newDim,bondDimension);
		for(s1=0;s1<spinSize;s1++)
		  M[s1][site+delta]=Mtemp*M[s1][site+delta];
	  } else {
		Mtemp=(svd.matrixU()*tempvec.asDiagonal()).block(0,0,bondDimension,newDim);
		for(s1=0;s1<spinSize;s1++)
		  M[s1][site+delta]=M[s1][site+delta]*Mtemp;
	  }
	  
	  //	  cerr << "Here e" << endl;

	  //Split out spin states on site
	  if(site==startsite) {
		if(leftToRight) {
		  Mtemp=svd.matrixU();
		  for(s1=0;s1<spinSize;s1++) {
			M[s1][site]=MatrixType::Zero(1,newDim);
			M[s1][site].block(0,0,1,Mtemp.cols())=Mtemp.row(s1);
		  }
		} else {
		  Mtemp=svd.matrixV().adjoint();
		  for(s1=0;s1<spinSize;s1++) {
			M[s1][site]=MatrixType::Zero(newDim,1);
			M[s1][site].block(0,0,Mtemp.rows(),1)=Mtemp.col(s1);
		  }
		}
	  } else if(leftToRight) {
		Mtemp=svd.matrixU().block(0,0,newDim*spinSize,newDim);
		for(s1=0;s1<spinSize;s1++) {
		  M[s1][site].resize(newDim,newDim);
		  for(j=0;j<M[0][site].rows();j++)
			M[s1][site].row(j)=Mtemp.row(j*spinSize+s1);
		} 
	  } else {
		Mtemp=svd.matrixV().adjoint().block(0,0,newDim,newDim*spinSize);
		for(s1=0;s1<spinSize;s1++) {
		  M[s1][site].resize(newDim,newDim);
		  for(j=0;j<M[0][site].cols();j++)
			M[s1][site].col(j)=Mtemp.col(j*spinSize+s1);
		}
	  }
	  //	  cerr << "Here f" << endl;
	}

	if(site!=endsite) {
	  cerr << "After loop, site=" << site << ", endsite=" << endsite << endl;
	  assert(site==endsite);
	}

	bondDimension=newDim;

	//Canonize the state (see below)
	double norm=0;
	for(s1=0;s1<spinSize;s1++) {
	  if(!leftToRight)
		Mtemp=M[s1][endsite]*M[s1][endsite].adjoint();
	  else
		Mtemp=M[s1][endsite].adjoint()*M[s1][endsite];
	  assert(Mtemp.rows()==1&&Mtemp.cols()==1);
	  norm+=abs(Mtemp(0,0));
	}
	norm=sqrt(norm);
	for(s1=0;s1<spinSize;s1++)
	  M[s1][endsite]=M[s1][endsite]/norm;

	isCanonical=true;

	leftToRight=!leftToRight;
	return 2*(1-sqrt(1-error)); //error
  }

  //Use SVD to contract the MPS to size newDim
  //May need to introduce variables in the MPS class to consider left/right canonical
  double SVDResize(int newDim,vector<vector<double> > & rhoReduced) {
	double error=0;

	assert(newDim>=spinSize);
	
	//First canonized/normalize
	error=zipIt(bondDimension,rhoReduced);
	if(error!=0) {
	  cerr << "rhoReduced=" << endl;
	  for(int j=0;j<rhoReduced.size();j++) {
		for(int k=0;k<rhoReduced[j].size();k++) {
		  cerr << rhoReduced[j][k] << ' ';
		}
		cerr << endl;
	  }
	  cerr << "error=" << error << endl;
	  assert(error==0);
	}

	//Then compress if necessary
	if(bondDimension < newDim)
	  return 0;
	error+=zipIt(newDim,rhoReduced);

	bondDimension=newDim;
	return error;
  }

  //Compress the state with an error cutoff
  double SVDResize(double error_thresh) {
	double error=0;
	
	//First canonized/normalize
	error=zipIt(0);

	//Then compress
	error+=zipIt(error_thresh);

	return error;
  }

  void getRhoReduced(vector<vector<double> > & rhoReduced) {
	zipIt(bondDimension,rhoReduced); //First canonize
	zipIt(bondDimension,rhoReduced); //Then get data
  }

  //Use this to apply an MPO that is stored as an MPS
  //|this>=mpo|this>
  void applyMPO(const MPSobcClass<scalar_type> & mpo) {
	assert(mpo.isMPO);
	assert(mpo.numSites==numSites);
	//If this is not an MPO, apply as usual
	int site, spin1, spin2, spin3;
	int newDim=mpo.bondDimension*bondDimension;
	vector< MatrixType > tempM(spinSize);
	if(!isMPO) {
	  assert(mpo.mposs==spinSize);

	  for(site=0;site<numSites;site++) {
		for(spin1=0;spin1<spinSize;spin1++) {
		  if(site==0)
			tempM[spin1]=MatrixType::Zero(1,newDim);
		  else if(site==numSites-1)
			tempM[spin1]=MatrixType::Zero(newDim,1);
		  else
			tempM[spin1]=MatrixType::Zero(newDim,newDim);

		  //Tensor product and contract over spin 2
		  for(spin2=0;spin2<spinSize;spin2++) 
			tempM[spin1]+=outer<MatrixType>(mpo.M[spin1+spinSize*spin2][site],M[spin2][site]);
		}

		for(spin1=0;spin1<spinSize;spin1++)
		  M[spin1][site]=tempM[spin1];
	  }
	} 
	//Otherwise, apply MPO to MPO (multiply operators)
	else {
	  assert(mpo.mposs==mposs);

	  for(site=0;site<numSites;site++) {
		for(spin1=0;spin1<mposs;spin1++) {
		  for(spin3=0;spin3<mposs;spin3++) {
			if(site==0)
			  tempM[spin1+mposs*spin3]=MatrixType::Zero(1,newDim);
			else if(site==numSites-1)
			  tempM[spin1+mposs*spin3]=MatrixType::Zero(newDim,1);
			else
			  tempM[spin1+mposs*spin3]=MatrixType::Zero(newDim,newDim);
			
			//Tensor product and contract over spin 2
			for(spin2=0;spin2<mposs;spin2++) 
			  tempM[spin1+mposs*spin3]+=outer<MatrixType>(mpo.M[spin1+mposs*spin2][site],M[spin2+mposs*spin3][site]);
		  }
		}

		for(spin1=0;spin1<mposs;spin1++)
		  for(spin3=0;spin3<mposs;spin3++)
			M[spin1+mposs*spin3][site]=tempM[spin1+mposs*spin3];
	  }
	}
	isCanonical=false;
	isNormalized=false;
	bondDimension=newDim;//It is now bigger
  }


  //return <a|this>
  scalar_type dotMPS(const MPSobcClass<scalar_type> & a) const {
	Matrix<scalar_type, Eigen::Dynamic, Eigen::Dynamic> Mold, Mnew;
	Mold.resize(1,1);
	Mold(0,0)=1;
	assert(numSites==a.numSites);
	assert(spinSize==a.spinSize);
	for(int site=0;site<numSites;site++) {
	  Mnew=a.M[0][site].adjoint()*Mold*M[0][site];
	  for(int spin=1;spin<spinSize;spin++)
		Mnew+=a.M[spin][site].adjoint()*Mold*M[spin][site];
	  Mold=Mnew;
	}
	assert(Mnew.cols()==1&&Mnew.rows()==1);
	return Mnew(0,0);
  }

  MPSobcClass & operator = (const MPSobcClass & b) {
	if(!b.isMPO)
	  InitMPS(b.numSites,b.bondDimension,b.spinSize);
	else
	  InitMPO(b.numSites,b.bondDimension,b.mposs);
    for (int s=0;s<spinSize;s++)
      for (int site=0;site<numSites;site++)
		M[s][site]=b.M[s][site];

	leftToRight=b.leftToRight;
	isCanonical=b.isCanonical;
	isNormalized=b.isNormalized;
	isMPO=b.isMPO;

	return *this;
  }

  scalar_type MPSval(const vector<int> & config) const {
	MatrixType Mtemp;
	assert(config.size()==numSites);
	assert(config[0]>=0&&config[0]<spinSize);
	Mtemp=M[config[0]][0];
	for(int site=1;site<numSites;site++) {
	  assert(config[site]>=0&&config[site]<spinSize);
	  Mtemp=Mtemp*M[config[site]][site];
	}
	assert(Mtemp.cols()==1&&Mtemp.rows()==1);
	return Mtemp(0,0);
  }

  scalar_type MPOval(const vector<int> & config1, const vector<int>  &config2) const {
	vector<int> config(config1.size());
	assert(isMPO);
	assert(config1.size()==config2.size());
	for(int j=0;j<config.size();j++)
	  config[j]=config1[j]+mposs*config2[j];
	return MPSval(config);
  }

  void allVals(const vector<vector<int> > & basis, VectorType & v) const {
	v=VectorType::Zero(basis.size());
	for(int j=0;j<basis.size();j++)
	  v(j)=MPSval(basis[j]);
  }

  void allVals(const vector<vector<int> > & basis, MatrixType & m) const {
	m=MatrixType::Zero(basis.size(),basis.size());
	for(int j=0;j<basis.size();j++)
	  for(int k=0;k<basis.size();k++)
		m(j,k)=MPOval(basis[j],basis[k]);
  }

  void pickRandomMPS() {
	for(int spin=0;spin<spinSize;spin++) {
	  for(int site=1;site<numSites-1;site++)
		M[spin][site]=MatrixType::Random(bondDimension,bondDimension);
	  M[spin][0]=MatrixType::Random(1,bondDimension);
	  M[spin][numSites-1]=MatrixType::Random(bondDimension,1);
	}
  }

  bool isIdentity(const MatrixType & mat) {
	int restrictedDim=-1;
	for(int j=0;j<mat.rows();j++) {
	  for(int k=0;k<mat.cols();k++) {
		if(j==k) {
		  if(abs(mat(j,k)-1.0) < 1e-9) {
			if(restrictedDim!=-1)
			  return false;
		  } else if(abs(mat(j,k)) < 1e-9) {
			restrictedDim=j;
		  } else {
			return false;
		  }
		}
		else if(abs(mat(j,k)) > 1e-9)
		  return false;
	  }
	}
	return true;
  }

  void printCanonical(ostream & out) {
	bool left, right;
	MatrixType mtemp;
	for(int site=0;site<numSites;site++) {
	  mtemp=M[0][site].adjoint()*M[0][site];
	  for(int spin=1;spin<spinSize;spin++)
		mtemp+=M[spin][site].adjoint()*M[spin][site];
	  left=isIdentity(mtemp);
	  mtemp=M[0][site]*M[0][site].adjoint();
	  for(int spin=1;spin<spinSize;spin++)
		mtemp+=M[spin][site]*M[spin][site].adjoint();
	  right=isIdentity(mtemp);
	  string leftstr=left?("is"):("is not");
	  string rightstr=right?("is"):("is not");
	  out << "Site " << site << ' ' <<  leftstr << " left canonical and " << rightstr << " right canonical" << endl;
	}
  }
};

double calcRenyiEntropy(const vector<double> & rhoRed, double alpha) {
  double entropy=0;
  if(abs(alpha-1)<1e-9) {
	//return von Neumann entropy
	for(int j=0;j<rhoRed.size();j++)
	  if(rhoRed[j]>0)
		entropy+=rhoRed[j]*log2(rhoRed[j]);
  } else {
	for(int j=0;j<rhoRed.size();j++)
	  if(rhoRed[j]>0)
		entropy+=pow(rhoRed[j],alpha);
	entropy=-log2(entropy)/(1-alpha);
  }
  return -entropy;
}

#endif
