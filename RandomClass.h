/*
 * RandomClass.h
 *
 *  Created on: Jun 26, 2008
 *      Author: bkclark
 */

#ifndef RANDOMCLASS_H_
#define RANDOMCLASS_H_
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
// #include "defs.h"
//A#define SIMPLE_SPRNG
//A#include <sprng.h>
#include <sys/time.h>
//#include "/usr/include/blitz/blitz.h"
//#include "/usr/include/blitz/array.h"
//using namespace blitz;
#define SEED 985456376
class RandomClass {
  double rand_cnt;
public:
  RandomClass(long int seed=-1)
  {
	rand_cnt=0;
	if(seed==-1) {
	  //A      init_sprng(DEFAULT_RNG_TYPE,SEED,SPRNG_DEFAULT); 
	  //    init_sprng(DEFAULT_RNG_TYPE,make_new_seed(),SPRNG_DEFAULT); 
	  timeval tim;
	  gettimeofday(&tim,NULL);
	  //Improve random seeding by starting from time in microseconds + time in seconds
	  srand48(tim.tv_sec+tim.tv_usec);
	} else{
	  srand48(seed);
	}
  }


  double get_rand_cnt() {
	return rand_cnt;
  }
	
	double ranf()
	{
	  rand_cnt+=1;
	  //A	  double val=sprng();
	  //A	  return val;
	  double val=drand48();
	  //	  cerr<<"Val is "<<val<<endl;
	 
	  return val;
	   //	  return rand()/(double)RAND_MAX;

	}

	int randInt(int aboveMax)
	{
	  return (int)floor(ranf()*aboveMax);
	  //	  return rand() % aboveMax;
	}

/* 	TinyVector<int,2> randInt(TinyVector<int,2> aboveMax){ */
/* 	  cerr<<"BEING CALLED"<<endl; */
/* 	  cerr<<aboveMax<<endl; */
/* 	  TinyVector<int,2> temp; */

/* 	  temp(0)=randInt(aboveMax(0)); */
/* 	  temp(1)=randInt(aboveMax(1)); */
/* 	  cerr<<"done"<<endl; */
/* 	  return temp; */
/* 	} */

	double gauss_rand(double sigma)
	{
	  double x1, x2, w, y1, y2;

	  do {
	    x1 = 2.0 * ranf() - 1.0;
	    x2 = 2.0 * ranf() - 1.0;
	    w = x1 * x1 + x2 * x2;
	  } while ( w >= 1.0 );

	  w = sqrt( (-2.0 * log( w ) ) / w );
	  y1 = x1 * w;
	  y2 = x2 * w;
	  return y1*sigma;
	}

/* 	dVec gauss_rand_vec(double sigma) */
/* 	{ */
/* 	  dVec newVec; */
/* 	  for (int dim=0;dim<NDIM;dim++) */
/* 	    newVec[dim]=gauss_rand(sigma); */
/* 	  return newVec; */

/* 	} */

/* 	dVec uniform_rand_vec(double sigma) */
/* 	{ */
/* 	  dVec newVec; */
/* 	  for (int dim=0;dim<NDIM;dim++) */
/* 	    newVec[dim]=(ranf()-0.5)*sigma; */
/* 	  return newVec; */

/* 	} */



};

#endif /* RANDOMCLASS_H_ */
