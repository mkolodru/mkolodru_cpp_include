#ifndef MK_SVD_H
#define MK_SVD_H

#include <Eigen/Eigen>
#include <complex>
#include <cstdlib>
#include <cmath>
#include "mkbasic.h"

using namespace Eigen;
using namespace std;

template<class scalar_type>
class MKSVD 
{
  typedef Matrix<scalar_type,Eigen::Dynamic,Eigen::Dynamic> MatrixClass;
  MatrixClass U;
  MatrixClass V;
  VectorXd S, Sinv;
  
  bool find_sqrt(const VectorXd & s) {
	bool worked=true;
	S=0*s;
	for(int j=0;j<S.rows();j++) {
	  S(j)=sqrt(max(-s(j),1e-30));
	}
	return worked;
  }

public:
  MatrixClass matrixU()
  {
    return U;
  }
  MatrixClass matrixV()
  {
    return V;
  }
  VectorXd singularValues()
  {
    return S;
  }
  void compute(const MatrixClass &A)
  {
	bool adjoint=false;
	if(A.rows() > A.cols()) 
	  adjoint=true;

	if(adjoint) {
	  U=-A.adjoint()*A;
	  SelfAdjointEigenSolver<MatrixClass> eigsolve(U);
	  V=eigsolve.eigenvectors();
	  if(!find_sqrt(eigsolve.eigenvalues())) {
		cerr << "S^2 had negative eigenvectors" << endl;
		eout(A);
		eout(-A.adjoint()*A);
		eout(S);
		eout(V);
		exit(1);
	  }
	  Sinv=S.cwise().inverse();
	  U=A*V*Sinv.asDiagonal();
	} else {
	  V=-A*A.adjoint();
	  SelfAdjointEigenSolver<MatrixClass> eigsolve(V);
	  U=eigsolve.eigenvectors();
	  if(!find_sqrt(eigsolve.eigenvalues())) {
		cerr << "S^2 had negative eigenvectors" << endl;
		eout(A);
		eout(-A.adjoint()*A);
		eout(eigsolve.eigenvalues());
		eout(S);
		eout(U);
		exit(1);
	  }
	  Sinv=S.cwise().inverse();
	  V=A.adjoint()*U*Sinv.asDiagonal();
	}
  }
};

#endif
