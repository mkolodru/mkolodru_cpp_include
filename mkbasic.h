#ifndef MKBASIC_H
#define MKBASIC_H

#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
#include <cassert>
#include <list>
#include <vector>
using namespace std;
#define JMAX 100
#define HISTBINS 1000

#define eout(a) cerr<<#a<<'='<<a<<endl

int histBin(double val, double begin, double end, int nbins=HISTBINS) {
  int bin=(int)((val-begin)/(end-begin)*nbins);
  if(bin>=nbins)
    bin=nbins-1;
  return bin;
}

void nrerror(string error_text) {
  cerr << error_text << endl;
  assert(1==2);
}


double rtbis(double (*func)(double), double x1, double x2, double xacc) {
  int j;
  double dx,f,fmid,xmid,rtb;

  f=(*func)(x1);
  fmid=(*func)(x2);
  if (f*fmid >= 0.0) nrerror("Root must be bracketed for bisection in rtbis");
  rtb = f < 0.0 ? (dx=x2-x1,x1) : (dx=x1-x2,x2); //Orient the search so that f>0 lies at x+dx.
  //Bisection loop.
  for (j=1;j<=JMAX;j++) {
    fmid=(*func)(xmid=rtb+(dx *= 0.5));
    if (fmid <= 0.0) rtb=xmid;
    if (fabs(dx) < xacc || fmid == 0.0) return rtb;
  }
  nrerror("Too many bisections in rtbis");
  return 0.0;
}

void Tokenize(const string& str,
			  vector<string>& tokens,
			  const string& delimiters = " ")
{
  // Skip delimiters at beginning.
  string::size_type lastPos = str.find_first_not_of(delimiters, 0);
  // Find first "non-delimiter".
  string::size_type pos     = str.find_first_of(delimiters, lastPos);
  
  while (string::npos != pos || string::npos != lastPos)
    {
      // Found a token, add it to the vector.
      tokens.push_back(str.substr(lastPos, pos - lastPos));
      // Skip delimiters.  Note the "not_of"
      lastPos = str.find_first_not_of(delimiters, pos);
      // Find next "non-delimiter"
      pos = str.find_first_of(delimiters, lastPos);
    }
}

void copyFile(char * before, char * after) {
  ifstream f1(before, fstream::binary);
  ofstream f2(after, fstream::trunc|fstream::binary);
  if(f1)
	f2 << f1.rdbuf();
}



#endif
