#ifndef MKMATH_H
#define MKMATH_H

#include <vector>
#include <list>
#include <cmath>
#include <cassert>
using namespace std;

double sgn(double d) {
  return d>0?1.:-1.;
}

double acot(double d) {
  return atan(1.0/d);
}

double dot(const vector<double> & a, const vector<double> & b) {
  assert(a.size()==b.size());
  int j, len=a.size();
  double d=0;
  for(j=0;j<len;j++)
    d+=a[j]*b[j];
  return d;
}

double factorial(double x) {
  double v=1;
  while(x>1) {
    v*=x;
    x-=1;
  }
  return v;
}

template<class type>
type ipow(type d, int p) {
  if(p==0)
    return 1;
  if(p<0)
    return type(1.0)/ipow(d,-p);
  type v=d;
  for(int j=1;j<p;j++)
    v*=d;
  return v;
}

double csch(double x) {
  return 1/sinh(x);
}

double sech(double x) {
  return 1/cosh(x);
}

double coth(double x) {
  return 1/tanh(x);
}

double binomial(double a, double b) {
  int lower=int(round(b));
  double val=1;
  for(int j=0;j<lower;j++) {
    val*=(a/b);
    a-=1;
    b-=1;
  }
  return val;
}

double find_root(double (*fn)(double), double a, double b, double val=0) {
  assert(sgn((*fn)(a)-val)!=sgn((*fn)(b)-val));
  
  double c=(a+b)/2.;
  while(a!=b&&a!=c&&b!=c) {
    if(sgn((*fn)(a)-val)==sgn((*fn)(c)-val))
      a=c;
    else
      b=c;
    c=(a+b)/2.;
  }
  return c;
}

#endif
