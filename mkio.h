#ifndef MK_IO_H
#define MK_IO_H

#include <iostream>
#include <fstream>
#include <Eigen/Eigen>
#include <Eigen/Dense>
#include "MPSobcClass.h"
#include <complex>

using namespace std;
using namespace Eigen;

ostream & operator << (ostream & out, const MatrixXcd & m) {
  //  out << m.rows()<<'x'<<m.cols()<<endl;
  for(int j=0;j<m.rows();j++) {
	for(int k=0;k<m.cols();k++) {
	  out << m(j,k) << ' ';
	}
	out << endl;
  }
  return out;
}

template<class scalar_type>
void read_matrix(string obsfile, Matrix<scalar_type,Dynamic,Dynamic> & H) {
  int j,k,NCONFIG;
  ifstream tempIn(obsfile.c_str());
  assert(tempIn);
  tempIn >> NCONFIG;
  H=MatrixXd::Zero(NCONFIG,NCONFIG);
  for(j=0;j<NCONFIG;j++)
	for(k=0;k<NCONFIG;k++)
	  tempIn >> H(j,k);
  tempIn.close();
}

// Subroutine to read diagonalized Hamiltonian from HDF5 file. Will become part of 
// codebase for doing other HDF5 manipulations.
void read_H_diag_hdf5(string filename, vector<double> & E, vector<complex<double> > & U) {
  int FAIL = -1;

  // Code for complex type from https://www.hdfgroup.org/HDF5/doc/H5.user/Datatypes.html
  typedef struct {
    double re;   /*real part*/
    double im;   /*imaginary part*/
  } complex_t;
  
  // First read E and set matrix sizes
  hsize_t mat_sz[1], max_sz[1];
    
  hid_t h5_file=H5Fopen(filename.c_str(),H5F_ACC_RDONLY,H5P_DEFAULT);

  hid_t dataset_E = H5Dopen2(h5_file, "E", H5P_DEFAULT); 
  assert(dataset_E != FAIL);
  H5Sget_simple_extent_dims(H5Dget_space(dataset_E), mat_sz, max_sz);

  int basis_sz=mat_sz[0];
  E.resize(basis_sz);
  U.resize(basis_sz*basis_sz);

  hid_t memtype_E = H5T_NATIVE_DOUBLE;
  assert(H5Dread(dataset_E, memtype_E, H5S_ALL, H5S_ALL, H5P_DEFAULT, E.data()) != FAIL);

  // Then read U
  hid_t dataset_U = H5Dopen2(h5_file, "U", H5P_DEFAULT); 
  assert(dataset_U != FAIL);

  // According to http://docs.h5py.org/en/latest/config.html, by default complex numbers are stored as ('r','i') by python
  hid_t memtype_U = H5Tcreate (H5T_COMPOUND, sizeof(complex_t));
  H5Tinsert (memtype_U, "r", HOFFSET(complex_t,re),
	     H5T_NATIVE_DOUBLE);
  H5Tinsert (memtype_U, "i", HOFFSET(complex_t,im),
	     H5T_NATIVE_DOUBLE);

  H5Sget_simple_extent_dims(H5Dget_space(dataset_U), mat_sz, max_sz);
  assert(mat_sz[0]==U.size());
  vector<complex_t> U_ct(U.size());
  assert(H5Dread(dataset_U, memtype_U, H5S_ALL, H5S_ALL, H5P_DEFAULT, U_ct.data()) != FAIL);
  complex<double>  imag_unit=complex<double> (0.,1.);
  for(int j=0;j<U.size();j++) 
    U[j]=U_ct[j].re+imag_unit*U_ct[j].im;

  // Then close the file
  assert(H5Dclose(dataset_E) != FAIL); 
  assert(H5Dclose(dataset_U) != FAIL); 
  H5Fclose(h5_file);
}

#endif
