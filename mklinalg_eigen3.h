#ifndef MK_LINALG_H
#define MK_LINALG_H

#include <Eigen/Eigen>
#include "mkbasic.h"
using namespace Eigen;

template<class MatrixType> 
MatrixType outer(const MatrixType & A, const MatrixType & B) {
  MatrixType mtemp(A.rows()*B.rows(),A.cols()*B.cols());
  for(int j=0;j<A.rows();j++)
	for(int k=0;k<A.cols();k++)
	  mtemp.block(j*B.rows(),k*B.cols(),B.rows(),B.cols())=A(j,k)*B;
  return mtemp;
}

//Return exp(c*H), where H must be self-adjoint
template<class scalar_type> 
Matrix<scalar_type,Dynamic,Dynamic> exp(const Matrix<scalar_type,Dynamic,Dynamic> & H, scalar_type c) {
  cerr << "About to do exp(c H), with c=" << c << ", H=" << endl;
  cerr << H << endl;
  assert(H.rows()==H.cols());

  typedef Matrix<scalar_type,Dynamic,Dynamic> MatrixType;
  typedef Matrix<scalar_type,Dynamic,1> VectorType;

  //Do it the diagonalization way
  SelfAdjointEigenSolver<MatrixType> eigsolve(H);
  MatrixType U=eigsolve.eigenvectors();
  eout(U);
  //  MatrixXd D=eigsolve.eigenvalues().asDiagonal();
  VectorXd Ed=eigsolve.eigenvalues();
  eout(Ed);
  VectorType E=(Ed.cast<scalar_type>()*c).array().exp();
  eout(E);
  //  MatrixType diff2=H-U*D*U.adjoint(); == 0 by testing
  MatrixType result=U*E.asDiagonal()*U.adjoint();
  eout(result);

  return result;
}

#endif
