#ifndef MK_SVD_H
#define MK_SVD_H

#include <Eigen/Eigen>
#include <Eigen/Dense>
#include <complex>
#include <cstdlib>

//Functions return U, D, and V
template<class scalar_type>
class MKSVD 
{
  typedef Matrix<scalar_type,Eigen::Dynamic,Eigen::Dynamic> MatrixClass;
  MatrixClass U;
  MatrixClass V;
  VectorXd diag;
  JacobiSVD<MatrixClass> svd;

  void stripZeros(const MatrixClass & a, MatrixClass & b) { 
	int j, rowcnt, colcnt;
	for(rowcnt=a.rows()-1;rowcnt>=0;rowcnt--) {
	  for(j=0;j<a.cols();j++)
		if(abs(a(rowcnt,j))>1e-15)
		  break;
	  if(j!=a.cols())
		break;
	}
	rowcnt++;
	for(colcnt=a.cols()-1;colcnt>=0;colcnt--) {
	  for(j=0;j<a.rows();j++)
		if(abs(a(j,colcnt))>1e-15)
		  break;
	  if(j!=a.rows())
		break;
	}
	colcnt++;
	b=a.block(0,0,rowcnt,colcnt);
  }

public:
  MatrixClass matrixU()
  {
	return U;
  }
  MatrixClass matrixV()
  {
	return V;
  }
  VectorXd singularValues()
  {
	return diag;
  }
  void compute(const MatrixClass & mat_orig)
  {
	int j,k;
	int m,n,d;

	int m_orig=mat_orig.rows();
	int n_orig=mat_orig.cols();
	int d_orig=min(m_orig,n_orig);

	MatrixClass mat;
	stripZeros(mat_orig,mat);

	/*
	//I know that m < n works, otherwise we have some issues
	bool adjointed;
	if(mat.rows() <= mat.cols()) {
	  adjointed=false;
	  m=mat.rows();
	  n=mat.cols();
	} else {
	  adjointed=true;
	  m=mat.cols();
	  n=mat.rows();
	}

	if(m==0 || n==0) {
	  cerr << "Stripped matrix is too small" << endl;
	  cerr << "mat_orig=" << endl;
	  cerr << mat_orig << endl;
	  cerr << "mat=" << endl;
	  cerr << mat << endl;
	  exit(1);
	}

	assert(m<=n);*/

	m=mat.rows();
	n=mat.cols();

	//Sizes should be as follows:
	//U = m x m, V = n x m, D = m x m
	U=MatrixClass::Zero(m_orig,d_orig);
	V=MatrixClass::Zero(n_orig,d_orig);
	diag=VectorXd::Zero(d_orig);

	svd.compute(mat,ComputeThinU|ComputeThinV);
	d=svd.nonzeroSingularValues();
	assert(d>0);

	U.block(0,0,m,d)=svd.matrixU().block(0,0,m,d);
	V.block(0,0,n,d)=svd.matrixV().block(0,0,n,d);
	diag.block(0,0,d,1)=svd.singularValues().block(0,0,d,1);
	
	if(d!=min(m,n)) {
	  eout(d);
	  eout(m);
	  eout(n);
	  eout(mat);
	  eout(svd.matrixU());
	  eout(svd.matrixV());
	  eout(svd.singularValues());
	}

	/*
	if(adjointed) {
	  svd.compute(mat.adjoint(),ComputeThinU|ComputeThinV);
	  V.block(0,0,m,m)=svd.matrixU();
	  U.block(0,0,n,m)=svd.matrixV();
	  diag.block(0,0,m,1)=svd.singularValues();
	}
	else {
	  svd.compute(mat,ComputeThinU|ComputeThinV);
	  U.block(0,0,m,m)=svd.matrixU();
	  V.block(0,0,n,m)=svd.matrixV();
	  diag.block(0,0,m,1)=svd.singularValues();
	}
	*/
  }
};
#endif
