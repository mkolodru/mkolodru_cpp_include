#ifndef MK_SVD_H
#define MK_SVD_H

#include <Eigen/Eigen>
#include <Eigen/SVD>
#include <complex>
#include <cstdlib>

//Use zgesdd - Note: Even after culling zeros, this algorithm still routinely fails to converge
/*
#define ZGE_SDD
#undef ZGE_SVD
*/

//Use zgesvd

#define ZGE_SVD
#undef ZGE_SDD


using namespace Eigen;
using namespace std;

struct _dcomplex { double re, im; };
typedef struct _dcomplex dcomplex;

extern "C" void
zgesvd_ (char *JOBU, char* JOBVT, int *M, int *N,
		 dcomplex *A, int *LDA, double *S, dcomplex *U,
		 int *LDU, dcomplex *VT, int *LDVT, dcomplex *work,
		 int *LWORK, double *RWORK, int *INFO);

extern "C" void
zgesdd_ ( char* jobz, int* m, int* n, dcomplex* a,
		  int* lda, double* s, dcomplex* u, int* ldu, dcomplex* vt, int* ldvt,
		  dcomplex* work, int* lwork, double* rwork, int* iwork, int* info );

//Using Eigen::SVD smartly
//Should decompose the matrix as M=U D VT
//Functions return U, D, and V
template<class scalar_type>
class MKSVD 
{
  typedef Matrix<scalar_type,Eigen::Dynamic,Eigen::Dynamic> MatrixClass;

public:
  MatrixClass U;
  MatrixClass V;
  VectorXd diag;
  MatrixClass matrixU()
  {
    return U;
  }
  MatrixClass matrixV()
  {
    return V;
  }
  VectorXd singularValues()
  {
    return diag;
  }
  void compute(MatrixClass &M)
  {
	Eigen::SVD<MatrixClass> svd;
	if(M.rows() >= M.cols()) {
	  svd.compute(M);
	  U=svd.matrixU();
	  diag=svd.singularValues();
	  V=svd.matrixV();
	} else {
	  svd.compute(M.adjoint());
	  U=svd.matrixV();
	  diag=svd.singularValues();
	  V=svd.matrixU();
	}
  }
};

//Overload if using complex<double>, since that requires a different function

//Using zgesdd
#ifdef ZGE_SDD
template<>
class MKSVD<complex<double> > 
{
  typedef MatrixXcd MatrixClass;
  MatrixClass U;
  MatrixClass V;
  VectorXd diag;
  double * s;
  dcomplex * u;
  dcomplex * vt;
  dcomplex * a;
  int * iwork;
  dcomplex * work;
  double * rwork;
  int maxdim, lwork;
  
  void print_matrix( char* desc, int m, int n, dcomplex* a, int lda ) {
	int i, j;
	printf( "\n %s\n", desc );
	for( i = 0; i < m; i++ ) {
	  for( j = 0; j < n; j++ )
		printf( " (%6.2f,%6.2f)", a[i+j*lda].re, a[i+j*lda].im );
	  printf( "\n" );
	}
  }

  void print_rmatrix( char* desc, int m, int n, double* a, int lda ) {
	int i, j;
	printf( "\n %s\n", desc );
	for( i = 0; i < m; i++ ) {
	  for( j = 0; j < n; j++ ) printf( " %6.2f", a[i+j*lda] );
	  printf( "\n" );
	}
  }
  void resize_work() {
	kill_work();
	work = new dcomplex[lwork];
  }
  void kill_work() {
	if(work!=NULL)
	  delete [] work;
  }
  void resize_arrays() {
	kill_arrays();
	s=new double[maxdim];
	rwork=new double[5*maxdim*(maxdim)+7*maxdim];
	u=new dcomplex[maxdim*maxdim];
	vt=new dcomplex[maxdim*maxdim];
	a=new dcomplex[maxdim*maxdim];
	iwork=new int[8*maxdim];
  }
  void kill_arrays() {
	if(s!=NULL) {
	  delete [] s;
	  delete [] u;
	  delete [] vt;
	  delete [] a;
	  delete [] iwork;
	  delete [] rwork;
	}
  }

  void stripZeros(const MatrixClass & a, MatrixClass & b) { 
	int j, rowcnt, colcnt;
	for(rowcnt=a.rows()-1;rowcnt>=0;rowcnt--) {
	  for(j=0;j<a.cols();j++)
		if(abs(a(rowcnt,j))>1e-15)
		  break;
	  if(j!=a.cols())
		break;
	}
	rowcnt++;
	for(colcnt=a.cols()-1;colcnt>=0;colcnt--) {
	  for(j=0;j<a.rows();j++)
		if(abs(a(j,colcnt))>1e-15)
		  break;
	  if(j!=a.rows())
		break;
	}
	colcnt++;
	b=a.block(0,0,rowcnt,colcnt);
  }

public:
  MKSVD<complex<double> > () {
	s=rwork=NULL;
	u=vt=a=work=NULL;
	iwork=NULL;
	maxdim=0;
	lwork=0;
  }
  ~MKSVD<complex<double> > () {
	kill_work();
	kill_arrays();
  }
  MatrixClass matrixU()
  {
	return U;
  }
  MatrixClass matrixV()
  {
	return V;
  }
  VectorXd singularValues()
  {
	return diag;
  }
  void compute(const MatrixClass & mat_orig)
  {
	int j,k;
	int m,n,d;

	int m_orig=mat_orig.rows();
	int n_orig=mat_orig.cols();
	int d_orig=min(m_orig,n_orig);

	MatrixClass mat;
	stripZeros(mat_orig,mat);
	
	//I know that m < n works, otherwise we have some issues
	bool adjointed;
	if(mat.rows() <= mat.cols()) {
	  adjointed=false;
	  m=mat.rows();
	  n=mat.cols();
	} else {
	  adjointed=true;
	  m=mat.cols();
	  n=mat.rows();
	}

	if(m==0 || n==0) {
	  cerr << "Stripped matrix is too small" << endl;
	  cerr << "mat_orig=" << endl;
	  cerr << mat_orig << endl;
	  cerr << "mat=" << endl;
	  cerr << mat << endl;
	  exit(1);
	}

	assert(m<=n);
	int lda=m;
	int ldu=m;
	int ldvt=n;
  
	//Resize main arrays if necessary
	if(n > maxdim) {
	  maxdim=n;
	  resize_arrays();
	}

	//Sizes should be as follows:
	//U = m x m, V = n x m, D = m x m
	U=MatrixClass::Zero(m_orig,d_orig);
	V=MatrixClass::Zero(n_orig,d_orig);
	diag=VectorXd::Zero(d_orig);

	int info;
	dcomplex wkopt;
	char jobu, jobvt;
	jobu=jobvt='A';
  
	for(k=0;k<n;k++) {
	  for(j=0;j<m;j++) {
		a[k*m+j].re=real(adjointed?mat(k,j):mat(j,k));
		a[k*m+j].im=imag(adjointed?(-mat(k,j)):mat(j,k));
	  }
	}

	//Find optimal lwork
	int lwork_opt = -1;
	zgesdd_( &jobu, &m, &n, a, &lda, s, u, &ldu, vt, &ldvt, 
			 &wkopt, &lwork_opt, rwork, iwork, &info);
	lwork_opt = (int)wkopt.re;
	if(lwork < lwork_opt) {
	  lwork=lwork_opt;
	  resize_work();
	}

	//Compute the SVD
	zgesdd_( &jobu, &m, &n, a, &lda, s, u, &ldu, vt, &ldvt, 
			 work, &lwork_opt, rwork, iwork, &info);

	// Check for convergence 
	if( info > 0 ) {
	  printf( "The algorithm computing SVD failed to converge.\n" );
	  exit( 1 );
	}

	for(j=0;j<m;j++) {
	  for(k=0;k<m;k++) {
		if(!adjointed)
		  U(j,k)=complex<double>(u[j+k*m].re,u[j+k*m].im);
		else
		  V(j,k)=complex<double>(u[j+k*m].re,u[j+k*m].im);
	  }
	}
	for(j=0;j<m;j++) {
	  for(k=0;k<n;k++) {
		if(!adjointed)
		  V(k,j)=complex<double>(vt[j+k*n].re,-vt[j+k*n].im);
		else 
		  U(k,j)=complex<double>(vt[j+k*n].re,-vt[j+k*n].im);
	  }
	}
	for(j=0;j<m;j++)
	  diag(j)=s[j];

	//Copy the matrices for posterity
	/*
	for(j=0;j<m;j++)
	  for(k=0;k<d;k++)
		U(j,k)=complex<double>(u[j+k*m].re,u[j+k*m].im);
	for(j=0;j<d;j++)
	  for(k=0;k<n;k++)
		V(k,j)=complex<double>(vt[j+k*n].re,-vt[j+k*n].im);
	for(j=0;j<d;j++)
	  diag(j)=s[j];
	*/


	//	print_rmatrix( "Singular values", 1, m, s, 1 );
	//	cout << "diag=" << singularValues() << endl;

	//	print_matrix( "Left singular vectors (stored columnwise)", m, m, u, ldu );
	//	cout << "U=" << matrixU() << endl;

	//	print_matrix( "Right singular vectors (stored rowwise)", m, n, vt, ldvt );
	//	print_matrix( "Right singular vectors (stored rowwise)", n, n, vt, ldvt );
	//	cout << "VT=" << matrixV().adjoint() << endl;

	//	cout << "orig=" << mat << endl;
	//	cout << "U D VT = " << matrixU()*singularValues().asDiagonal()*matrixV().adjoint() << endl;
	//	MatrixClass diff=mat_orig - matrixU()*singularValues().asDiagonal()*matrixV().adjoint();
	//	cout << "max(abs(diff))=" << diff.cwise().abs().maxCoeff() << endl;
	//	cout << "diag=" << diag << endl;
	//	cout << "U=" << endl;
	//	cout << U << endl;
	//	cout << "VT=" << endl;
	//	cout << V.adjoint() << endl;
	//	cout << "U D VT = " << endl;
	//	cout << U*diag.asDiagonal()*V.adjoint() << endl;
	//	cout << "mat = " << endl;
	//	cout << mat << endl;

  }
};
#endif

//Using zgesvd
#ifdef ZGE_SVD
template<>
class MKSVD<complex<double> > 
{
  typedef MatrixXcd MatrixClass;
  MatrixClass U;
  MatrixClass V;
  VectorXd diag;
  double * s;
  dcomplex * u;
  dcomplex * vt;
  dcomplex * a;
  dcomplex * work;
  double * rwork;
  int maxdim, lwork;
  
  void print_matrix( char* desc, int m, int n, dcomplex* a, int lda ) {
	int i, j;
	printf( "\n %s\n", desc );
	for( i = 0; i < m; i++ ) {
	  for( j = 0; j < n; j++ )
		printf( " (%6.2f,%6.2f)", a[i+j*lda].re, a[i+j*lda].im );
	  printf( "\n" );
	}
  }

  void print_rmatrix( char* desc, int m, int n, double* a, int lda ) {
	int i, j;
	printf( "\n %s\n", desc );
	for( i = 0; i < m; i++ ) {
	  for( j = 0; j < n; j++ ) printf( " %6.2f", a[i+j*lda] );
	  printf( "\n" );
	}
  }
  void resize_work() {
	kill_work();
	work = new dcomplex[lwork];
  }
  void kill_work() {
	if(work!=NULL)
	  delete [] work;
  }
  void resize_arrays() {
	kill_arrays();
	s=new double[maxdim];
	rwork=new double[5*maxdim];
	u=new dcomplex[maxdim*maxdim];
	vt=new dcomplex[maxdim*maxdim];
	a=new dcomplex[maxdim*maxdim];
  }
  void kill_arrays() {
	if(s!=NULL) {
	  delete [] s;
	  delete [] u;
	  delete [] vt;
	  delete [] a;
	  delete [] rwork;
	}
  }

  void stripZeros(const MatrixClass & a, MatrixClass & b) { 
	int j, rowcnt, colcnt;
	for(rowcnt=a.rows()-1;rowcnt>=0;rowcnt--) {
	  for(j=0;j<a.cols();j++)
		if(abs(a(rowcnt,j))>1e-15)
		  break;
	  if(j!=a.cols())
		break;
	}
	rowcnt++;
	for(colcnt=a.cols()-1;colcnt>=0;colcnt--) {
	  for(j=0;j<a.rows();j++)
		if(abs(a(j,colcnt))>1e-15)
		  break;
	  if(j!=a.rows())
		break;
	}
	colcnt++;
	b=a.block(0,0,rowcnt,colcnt);
  }

public:
  MKSVD<complex<double> > () {
	s=rwork=NULL;
	u=vt=a=work=NULL;
	maxdim=0;
	lwork=0;
  }
  ~MKSVD<complex<double> > () {
	kill_work();
	kill_arrays();
  }
  MatrixClass matrixU()
  {
	return U;
  }
  MatrixClass matrixV()
  {
	return V;
  }
  VectorXd singularValues()
  {
	return diag;
  }
  void compute(const MatrixClass & mat_orig)
  {
	int j,k;
	int m,n,d;

	int m_orig=mat_orig.rows();
	int n_orig=mat_orig.cols();
	int d_orig=min(m_orig,n_orig);

	MatrixClass mat;
	stripZeros(mat_orig,mat);
	
	/*
	if(mat.rows()!=mat_orig.rows()) {
	  cout << "Reduced size from " << mat_orig.rows() << 'x' << mat_orig.cols() << " to " << mat.rows() << 'x' << mat.cols() << endl;
	}
	*/

	//I know that m < n works, otherwise we have some issues
	bool adjointed;
	if(mat.rows() <= mat.cols()) {
	  adjointed=false;
	  m=mat.rows();
	  n=mat.cols();
	} else {
	  adjointed=true;
	  m=mat.cols();
	  n=mat.rows();
	}

	if(m==0 || n==0) {
	  cerr << "Stripped matrix is too small" << endl;
	  cerr << "mat_orig=" << endl;
	  cerr << mat_orig << endl;
	  cerr << "mat=" << endl;
	  cerr << mat << endl;
	  exit(1);
	}

	assert(m<=n);
	int lda=m;
	int ldu=m;
	int ldvt=n;
  
	//Resize main arrays if necessary
	if(n > maxdim) {
	  maxdim=n;
	  resize_arrays();
	}

	//Sizes should be as follows:
	//U = m x m, V = n x m, D = m x m
	U=MatrixClass::Zero(m_orig,d_orig);
	V=MatrixClass::Zero(n_orig,d_orig);
	diag=VectorXd::Zero(d_orig);

	int info;
	dcomplex wkopt;
	char jobu, jobvt;
	jobu=jobvt='A';
  
	for(k=0;k<n;k++) {
	  for(j=0;j<m;j++) {
		a[k*m+j].re=real(adjointed?mat(k,j):mat(j,k));
		a[k*m+j].im=imag(adjointed?(-mat(k,j)):mat(j,k));
	  }
	}

	//Find optimal lwork
	int lwork_opt = -1;
	zgesvd_( &jobu, &jobvt, &m, &n, a, &lda, s, u, &ldu, vt, &ldvt, 
			 &wkopt, &lwork_opt, rwork, &info);
	lwork_opt = (int)wkopt.re;
	if(lwork < lwork_opt) {
	  lwork=lwork_opt;
	  resize_work();
	}

	//Compute the SVD
	zgesvd_( &jobu, &jobvt, &m, &n, a, &lda, s, u, &ldu, vt, &ldvt, 
			 work, &lwork_opt, rwork, &info);

	// Check for convergence 
	if( info > 0 ) {
	  printf( "The algorithm computing SVD failed to converge.\n" );
	  exit( 1 );
	}

	for(j=0;j<m;j++) {
	  for(k=0;k<m;k++) {
		if(!adjointed)
		  U(j,k)=complex<double>(u[j+k*m].re,u[j+k*m].im);
		else
		  V(j,k)=complex<double>(u[j+k*m].re,u[j+k*m].im);
	  }
	}
	for(j=0;j<m;j++) {
	  for(k=0;k<n;k++) {
		if(!adjointed)
		  V(k,j)=complex<double>(vt[j+k*n].re,-vt[j+k*n].im);
		else 
		  U(k,j)=complex<double>(vt[j+k*n].re,-vt[j+k*n].im);
	  }
	}
	for(j=0;j<m;j++)
	  diag(j)=s[j];
  }
};
#endif

#endif
